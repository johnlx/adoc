[[introduction]]
== Introduction

[[platform-purpose]]
=== Platform Purpose

The WaRP7 is the next generation Internet of Things (IoT) and Wearable’s
Reference Platform. WaRP7 is a powerful, low-cost platform designed for
a fast prototyping and reduces time to market. WaRP7 is optimized, comes
in a tiny form factor and yet flexible enough to offer all the
advantages of traditional development tools. It has been architected and
designed from the ground up to address key challenges in the IoT and
wearables markets, such as battery life, connectivity, user experience
and miniaturization. WaRP7 is based on the NXP i.MX 7Solo applications
processor that features an advanced implementation of the ARM®
Cortex®-A7 core, as well as the ARM® Cortex®-M4 core. It comes with
features such as on-board sensors, connectivity including NFC,
Bluetooth®, Bluetooth Smart and Wi-Fi® and on-board external LPDDR3
memory.

This document is intended as an introduction to the WaRP7 CPU Board and
IO Board hardware and focuses primarily on its initial setup and basic
usage.

[[kit-contents]]
=== Kit Contents

In the box you will find the following items as shown in **Figure 1**.

* WaRP7 CPU Board
* WaRP7 IO Board
* Lithium Polymer Battery
* Quick Start Guide

[[warp]]
.The display for the kit can be purchased separately (__details coming soon__). Check for availability at http://www.element14.com/warp7[www.element14.com/warp7]
image::media/warp.png[align=center]

[[getting-started-with-hardware]]
=== Getting Started with Hardware

* STEP 1: Connect NFC cables to IO board
+
[[nfc_connectors]]
.Step 1
image::media/nfc_connectors.png[align=center]

* STEP 2: Connect CPU board
+
[[cpu_board]]
.Step 2
image::media/cpu_board.png[align=center]

* STEP 3: Connect battery
+
[[battery1]]
.Step 3.1
image::media/battery1.png[align=center]
+
[[battery2]]
.Step 3.2
image::media/battery2.png[align=center]

* STEP 4: Connect LCD
+
[[lcd1]]
.Step 4.1
image::media/lcd1.png[align=center]
+
[[lcd2]]
.Step 4.2
image::media/lcd2.png[align=center]
+
[[lcd3]]
.Step 4.3
image::media/lcd3.png[align=center]

* STEP 5: Connect USB

[[warp_connected]]
.WaRP7 connected
image::media/warp_connected.png[align=center]
