[[linux-software]]
== Linux Software

*Note:*

Command shown with prefix *$* are to be run on host machine (like ubuntu,
etc.).

Command shown with prefix *=>* are to be run on u-boot prompt.

Command shown with prefix *~#* are to be run on board after linux up.

[[compilation]]
=== Compilation

*Steps:*

. Extract the kernel and use the commands below to compile:

  $ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- distclean
  $ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- warp7_defconfig
  $ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage
  $ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- modules
  $ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- imx7d-warp7-mipi-dsi.dtb

. Extract and compile imx_usb_loader-master.

. Run imx_usb to load u-boot for the first time in the board.

  $ cd imx_usb_loader-master
  $ make
  $ ./imx_usb u-boot.imx

. U-boot prompt will come (we are using minicom).

. Run on u-boot:

  => ums 0 mmc 0

. Now you will be able to see emmc as storage device on your computer.

. Use any standard utility to make partition table.

. Create 3 partitions (10MB (Fat32), 100MB (Fat32) and remaining space
as ext4).

. Copy zImage to the 100MB fat32 partition.

. Rename and copy *imx7d-warp7-mipi-dsi.dtb* as *warp7.dtb* to the
100MB fat32 partition.

. Copy the *rootfs_debian.tar.bz2* to the 3rd ext4 partition.

. Now install kernel modules to the rootfs as shown below:

  $ sudo make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- modules_install
INSTALL_MOD_PATH=/path_to_your_emmc_ext4_partition_mount

. Now unmount the partitions using "*sudo umount /dev/sdX*"
command.

. In u-boot prompt, hit **Ctrl+c**, to cancel the mounted mmc.

. In u-boot prompt, use *loadx* command to load *u-boot* to ram.
+
After loadx completed , press **ctrl+a**, then *s* to select xmodem for
u-boot transfer.
+
  => loadx
+
Save the uboot in emmc.
+
  => mmc write 0x80800000 2 0x2a6
+
*<CAUTION>*
+
. Now fuse the OTP. This command is one time only, be careful otherwise it
will brick the board.

  => fuse prog 1 3 10002820

. Set bootcmd environment params as below:

  => setenv bootcmd 'setenv mmcroot /dev/mmcblk2p3 rootwait rw;setenv
bootargs console=$\{console},$\{baudrate} root=$\{mmcroot};mw 30330218
0;fatload mmc 0:2 0x80800000 zImage;fatload mmc 0:2 0x83000000
warp7.dtb;bootz 0x80800000 - 0x83000000'
  => saveenv
  => run bootcmd
+
Now linux must be up and running.
+
. Using NET OVER USB, we build USB ethernet build as **module**.

Use commands below on the board to load the modules.

Note: using static IP in your host system. We are using as below in
*/etc/network/interfaces*:

*192.168.7.10,*

*255.255.255.0,*

*192.168.7.1*

  ~# insmod /lib/modules/4.1.15/kernel/drivers/usb/gadget/libcomposite.ko
  ~# insmod /lib/modules/4.1.15/kernel/drivers/usb/gadget/function/u_ether.ko
  ~# insmod /lib/modules/4.1.15/kernel/drivers/usb/gadget/function/usb_f_rndis.ko
  ~# insmod /lib/modules/4.1.15/kernel/drivers/usb/gadget/legacy/g_ether.ko
  ~# ifup usb0

[[testing]]
=== Testing

[[testing-audio]]
==== Testing Audio:

* Listen audio from warp7

Place an audio file (ex: input.wav) in home directory and run command below:

  ~# aplay input.wav

 * Record audio using warp7

Run command below, it will record audio file with 10 secs duration.

  ~# arecord -d 10 output.wav

* To change volume control of headphone

Example below shows how to change volume to 100%:

  ~# amixer set 'Headphone' 100%

* To change gain control of mic

Example below shows how to change gain control to 100%:

  ~# amixer set 'Mic' 100%

*Testing Battery Charger:*

Compile and run the "bc3770.c" from the utils folder.

[[test_battery_charger]]
.Testing Battery Charger
image::media/test_battery_charger.png[align=center]

[[testing-sensors]]
==== Testing Sensors:

The MPL3115A2 sensor consists of Pressure and Altimeter.

The FXOS8700CQR1 sensor consists of Acclerometer and Magnetometer.

The FXAS21002CQR1 sensor consists of Gyrometer.

Run "i2cdetect" command to scan i2c bus for devices.

*UU* shows devices already binded to platform/drivers.

[[test_sensors]]
.Testing Sensors
image::media/test_sensors.jpeg[align=center]

To detect sensor identity status we will check for "**WHOAMI**" register
values.

Execute commands below in terminal:

*MPL3115A2:*

  ~# i2cget -y 3 0x60 0x0C

*0xc4*

MPL3115A2 datasheet confirms the "WHOAMI" value.

*FXOS8700CQR1*

  ~# i2cget -y 3 0x1e 0x0D

*0xc7*

FXOS8700CQR1 datasheet confirms the "WHOAMI" value.

*FXAS21002CQR1*

  $ i2cget -y 3 0x20 0x0C

*0xd7*

FXAS21002CQR1 datasheet confirms the "WHOAMI" value.

[[sensors_hardware_detection]]
.Sensors Hardware Detection
image::media/sensors_hardware_detection.png[align=center]

With above detection we can confirm the sensor hardware is working.

Sample codes "detect_acclerometer.c", "detect_gyrometer.c" and
"detect_mpl3115.c" confirm the same.

[[sensors_detection]]
.Sensors Detection
image::media/sensors_detection.png[align=center]

We developed a simple bare-metal application in Linux to access MPL3115
sensor.

The same can be done for Android/Yocto platforms.

The code "**mpl3115_temperature.c**" can be compiled and tested to get
temperature values.

You will get result as below (with debug enabled):

[[temperature_test]]
.Temperature Test
image::media/temperature_test.png[align=center]

(Note: the code access */dev/i2c-3* interface, if *MPL3115A2* driver
enabled in kernel, the code will fail due to device blocking by driver).

With testing part over, we can proceed to further development using
kernel supported drivers.

Enable the supported drivers in kernel by setting the values below to
"**y**" in *.config* or "**make menuconfig**":

  # CONFIG_SENSORS_FXOS8700 is not set
  # CONFIG_SENSORS_FXAS2100X is not set
  # CONFIG_INPUT_MPL3115 is not set

to

  CONFIG_SENSORS_FXOS8700=y
  CONFIG_SENSORS_FXAS2100X=y
  CONFIG_INPUT_MPL3115=y

Applications need to be written from Android/Debian/Yocto interfaces to
access full functionality.

[[testing-wifi]]
==== Testing Wifi:

Broadcom bcmdhd getting loaded and wireless interface getting up suring
kernel boot.

This comes from enabling "**Broadcom FullMAC wireless cards support** "
in kernel source "Device Drivers >> Network Device Support > Wireless
Lan".

The firmware supporting "**BCM4339**" hardware is available in
"**/lib/firmware/bcm**" directory.

[[test_wifi]]
.Testing Wifi
image::media/test_wifi.png[align=center]

Automatic IP allocation using DHCP:

[[automatic_ip_allocation]]
.Automatic IP Allocation
image::media/automatic_ip_allocation.png[align=center]

Verify IP address:

[[verify_ip_address]]
.Verify IP Address
image::media/verify_ip_address.png[align=center]

Ping test:

[[ping_test]]
.Ping Test
image::media/ping_test.png[align=center]

Change setting for *essid* & *password* in "/etc/network/interfaces"
file.

  auto lo
  iface lo inet loopback

  auto wlan0
  iface wlan0 inet dhcp
    wpa-ssid "SSID_NAME"
    wpa-psk "PASSWORD"


wpa_supplicant config *"/etc/wpa_supplicant.conf"*

  ctrl_interface=/var/run/wpa_supplicant
  ctrl_interface_group=0
  update_config=1

  network={
    ssid=" SSID_NAME "
    scan_ssid=1
    key_mgmt=WPA-PSK2
    psk=" PASSWORD "
    priority=5
  }

[[testing-lcd]]
==== Testing LCD:

After boot up, go to /root/lcd and run the following command:

  ./framebuffer.out

[[framebuffer]]
.Framebuffer mapped to memory
image::media/framebuffer.png[align=center]

It will start the following sequence of displays.

[[display1]]
.Display 1
image::media/display1.png[align=center]

[[display2]]
.Display 2
image::media/display2.png[align=center]

[[display3]]
.Display 3
image::media/display3.png[align=center]

[[display4]]
.Display 4
image::media/display4.png[align=center]

[[display5]]
.Display 5
image::media/display5.png[align=center]

[[display6]]
.Display 6
image::media/display6.png[align=center]

[[display7]]
.Display 7
image::media/display7.png[align=center]
