[[hardware-overview]]
== Hardware Overview

[[cpu-board]]
=== CPU Board

[[cpu]]
==== CPU

Main Processor: NXP MCIMX7S3DVK08SA - i.MX7S with 12mm x 12mm MAPBGA,
0.4mm.

The i.MX7S applications processor has an ARM Cortex-A7 core and an
ARM Cortex-M4. The device is targeted for IoT, Wearable and general
embedded markets.

[[soc_diagram]]
.iMX7S SoC Diagram
image::media/soc_diagram.jpeg[align=center]

[[memory-emcp-lpddr3-and-emmc]]
==== Memory eMCP – LPDDR3 and eMMC

The CPU Board features a Multi-Chip Package Memory
08EMCP04-EL3AV100-C30U from Kingston which combines 8GB eMMC and 512MB
Low Power DDR3 synchronous dynamic RAM. This comes in 221-ball FBGA
package.

[[emcp_diagram]]
.Kingston eMCP Block Diagram
image::media/emcp_diagram.png[align=center]

[[video-and-display]]
==== Video and Display

The WaRP7 CPU board provides output video from MIPI-DSI and accepts
input through MIPI-CSI.

[[mipi-dsi]]
===== MIPI-DSI

The CPU board includes a MIPI-DSI connector for outputting the
video from the i.MX7S MIPI-DSI PHY via the MIPI-DSI interface.

[[mipi-dsi_connector]]
.MIPI-DSI connector
image::media/mipi-dsi_connector.png[align=center]

[[capacitive-touch-screen]]
====== Capacitive Touch Screen

Capacitive touch screen is supported by I2C via touch screen port.

[[touch_screen_interface]]
.Touch Screen Interface
image::media/touch_screen_interface.png[align=center]

[[mipi-csi]]
===== MIPI-CSI

The CPU board includes a MIPI-CSI camera connector for connecting a CSI
camera module.

[[mipi_csi_connector]]
.MIPI CSI connector
image::media/mipi_csi_connector.png[align=center]

[[connectivity]]
==== Connectivity

The WaRP7 board will provide a number of connectivity including Wi-Fi,
Bluetooth, Bluetooth (BLE), and USB-OTG. There will be provision for NFC
as a passive tag primarily for Bluetooth pairing.

[[wi-fibluetooth]]
===== Wi-Fi/Bluetooth

The Murata Type 1DX module is an ultra-small module that includes 2.4GHz
WLAN IEEE 802.11b/g/n and Bluetooth Version 4.1 plus EDR functionality. Based on
Broadcom BCM4343W, the module provides high-efficiency RF front end
circuits.

[[bcm4343w_module]]
.Murata 1DX module
image::media/bcm4343w_module.jpeg[align=center]

[[design_1dx]]
.Design implementation of 1DX
image::media/design_1dx.png[align=center]

[[usb-otg]]
===== USB-OTG

The CPU board provides an USB micro-AB connector to support USB-OTG
function powered by the by USB OTG1 module on i.MX7S.

[[nfc]]
===== NFC

The board provides support for NFC using the NXP NT3H1101W0FHK. In
addition to the passive NFC Forum compliant contactless interface, the
IC features an I2C contact interface, which can communicate with i.MX7
if NTAG I2C is powered from an external power supply. An additional
externally powered SRAM mapped into the memory allows a fast data
transfer between the RF and I2C interfaces and vice versa, without the
write cycle limitations of the EEPROM memory.

[[nfc_schematic]]
.NFC circuitry
image::media/nfc_schematic.png[align=center]

[[power-management]]
==== Power Management

[[power-management-ic]]
===== Power Management IC

The NXP PF3000 power management integrated circuit (PMIC) features a
configurable architecture that supports numerous outputs with various
current ratings as well as programmable voltage and sequencing. This
enables the PF3000 to power the core processor, external memory and
peripherals to provide a single-chip system power solution.

[[pf3000_diagram]]
.PF3000 Functional Block diagram
image::media/pf3000_diagram.jpeg[align=center]

[[power-tree-design]]
===== Power Tree Design

The usage of PF3000 output is as shown in <<PF3000 Output Power Up Sequence and Usage>> below.

.PF3000 Output Power Up Sequence and Usage

[cols=",,,,",options="header",]
|=======================================================================
a|
*PF3000*

*Channel*

 |*Voltage* |*Power up sequence* a|
*Output*

*Current*

 |*i.MX7 Power Rail*
|SW1A |1.15 V |1 |1000 mA |VDD_ARM

|SW1B |1.15 V |1 |1750 mA |VDD_SOC

|SW2 |1.8 V |2 |1250 mA a|
VDDA_1P8_IN FUSE_FSOURCE VDD_XTAL_1P8

VDD_ADC1_1P8

VDD_ADC2_1P8

VDD_TEMPSENOR_1P8

|SW3 |1.5 V |3 |1500 mA |NVCC_DRAM NVCC_DRAM_CKE

|VSNVS |3.0 V |0 |1 mA |VDD_SNVS_IN

|SWBST | |- |600 mA |

|VREFDDR | |3 |10 mA |DRAM_VREF

|VLDO1 |1.8 V |2 |100 mA |VDD_LPSR_IN

|VLDO2 |1.2 V |- |250 mA |

|VLDO3 |1.8 V |2 |100 mA |NVCC_GPIO1/2

|VLDO4 |1.8 V |- |350 mA |

|V33 |3.15 V |2 |350 mA |NVCC_xxx VDD_USB_OTG1_3P3_IN
VDD_USB_OTG2_3P3_IN

|VCC_SD |3.15 V |3 |100 mA |NVCC_SD2
|=======================================================================

The following i.MX7S power rails must use the internal LDO outputs.

.iMX7S Power Rails – Internal LDO

[cols=",",options="header",]
|=================================================
|*i.MX7S internal LDO output* |*i.MX7S Power Rail*
|VDDD_1P0_CAP a|
VDD_MIPI_1P0

PCIE_VP PCIE_VP_RX PCIE_VP_TX

|VDDA_PHY_1P8 a|
VDDA_MIPI_1P8

PCIE_VPH PCIE_VPH_RX PCIE_VPH_TX

|VDD_1P2_CAP |USB_VDD_H_1P2
|=================================================

[[battery-charger]]
===== Battery Charger

The NXP BC3770 is a fully programmable switching charger with dual-path
output for single-cell Li-Ion and Li-Polymer battery. The dual-path
output allows mobile applications with a fully discharged battery to
boot up the system.

* High efficiency and switch-mode operation reduces heat dissipation and
allows higher current capability for a given package size.
* Single input with a 20V withstanding input and charges the battery
with an input current up to 2A.
* Charging parameters and operating modes are fully programmable over an
I2C Interface that operates up to 400 kHz.
* Highly integrated featuring OVP and Power FETs.
* Supports 1.5 MHz switching capabilities.

[[io-board]]
=== IO Board

[[audio]]
==== Audio

The IO board includes the Freescale SGTL5000 – an ultra-low power audio
codec with MIC In and Line Out capability.

[[audio_codec]]
.Freescale SGTL5000 Audio Codec
image::media/audio_codec.png[align=center]

[[sensors]]
==== Sensors

The WaRP7 board will include three sensors: altimeter, accelerometer and
gyroscope. These three sensor chips share the I2C bus on i.MX7S. The
sensors interrupts are wired to the processor as OR circuit. The
software will determine which device asserted the interrupt.

[[altimeter]]
===== Altimeter

The board features NXP’s MPL3115A2 precision altimeter. The MPL3115A2 is
a compact piezoresistive absolute pressure sensor with an I2C interface.
MPL3115 has a wide operating range of 20kPa to 110 kPa, a range that
covers all surface elevations on Earth. The fully internally compensated
MEMS in conjunction with an embedded high resolution 24-bit equivalent
ADC provide accurate pressure [Pascals] / altitude [meters] and
temperature [degrees Celsius] data.

[[mpl33115a2_diagram]]
.MPL3115A2 Block Diagram
image::media/mpl33115a2_diagram.jpeg[align=center]

[[altimeter_schematics]]
.Altimeter schematics
image::media/altimeter_schematics.png[align=center]

[[accelerometer-and-magnetometer]]
===== Accelerometer and Magnetometer

The board also features FXOS8700CQ 6-axis sensor combines
industry-leading 14-bit accelerometer and 16-bit magnetometer sensors in
a small 3 x 3 x 1.2 mm QFN plastic package.

[[accelerometer_magnetometer_diagram]]
.FXOS8700CQ – Accelerometer/Magnetometer Block Diagram
image::media/accelerometer_magnetometer_diagram.jpeg[align=center]

[[accelerometer_magnetometer_schematics]]
.Accelerometer/Magnetometer schematics
image::media/accelerometer_magnetometer_schematics.png[align=center]

[[gyroscope]]
===== Gyroscope

The IO board also features the NXP’s 3-axis digital gyroscope -
FXAS21002.

[[gyroscope_diagram]]
.FXAS21002 Gyroscope Block Diagram
image::media/gyroscope_diagram.jpeg[align=center]

[[gyroscope_schematics]]
.Gyroscope schematics
image::media/gyroscope_schematics.png[align=center]

[[peripheral-expansion-port]]
==== Peripheral Expansion Port

The board provides expansion headers compatible with the *mikroBUS^TM^*
socket connection standard for accessing the following communication
modules on i.MX7S:

* I2C
* SPI
* PWM
* UART
* GPIO
